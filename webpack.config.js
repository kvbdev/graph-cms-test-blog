const path = require('path')
const webpack = require('webpack')

const HtmlWebPackPlugin = require('html-webpack-plugin')

const htmlPlugin = new HtmlWebPackPlugin({
  template: './src/index.html',
  filename: './index.html'
})

module.exports = (env, options) => {
  return {
    entry: './src/client.js',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'static/app.bundle.js'
    },
    optimization: {
      splitChunks: {
        chunks: 'all'
      }
    },
    devtool: options.mode === 'development' ? 'eval-source-map' : 'source-map',
    devServer: {
      contentBase: './dist',
      hot: true
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          resolve: {
            extensions: ['.js', '.jsx']
          },
          use: {
            loader: 'babel-loader'
          }
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader']
        }
      ]
    },
    plugins: options.mode === 'development' ? [
      htmlPlugin,
      new webpack.HotModuleReplacementPlugin()
    ] : [
      htmlPlugin
    ]
  }
}
