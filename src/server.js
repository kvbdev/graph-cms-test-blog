import 'babel-polyfill'
import React from 'react'
import fs from 'fs'
import path from 'path'
import { ApolloProvider, renderToStringWithData } from 'react-apollo'
import { ServerStyleSheet } from 'styled-components'
import express from 'express'
import { StaticRouter } from 'react-router-dom'
import { Routes } from './Routes'
import { App } from './App'
import { createServerClient } from './configureApollo'

const app = express()

app.use('/static', express.static(path.join(process.cwd(), 'dist/static')))
app.use(async (req, res) => {
  const client = createServerClient()
  const context = {}

  const RouterInstance = (
    <App>
      <ApolloProvider client={client}>
        <StaticRouter location={req.url} context={context}>
          <Routes />
        </StaticRouter>
      </ApolloProvider>
    </App>
  )

  const sheet = new ServerStyleSheet()
  const content = await renderToStringWithData(sheet.collectStyles(RouterInstance))
  const initialState = client.extract()
  const styles = sheet.getStyleElement()

  const indexFile = path.resolve('dist/index.html')
  fs.readFile(indexFile, 'utf8', (err, data) => {
    if (err) {
      console.error('Something went wrong:', err)
      return res.status(500).send('Oops, better luck next time!')
    }

    data = data.replace(
      /<section id="app"><\/section>/,
      `<section id="app">${content}</section><script>
        window.__APOLLO_STATE__=${JSON.stringify(initialState).replace(/</g, '\\u003c')}
      </script>`)

    data = data.replace(/<style><\/style>/, styles)

    res.status(200)
    res.send(data)
    res.end()
  })
})

app.listen(8080, '0.0.0.0', () => {
  console.log(`😎 Server is listening on port 8080`)
})
