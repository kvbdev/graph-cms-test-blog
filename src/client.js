import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo'
import { App } from './App'
import { Routes } from './Routes'
import { createBrowserClient } from './configureApollo'

const client = createBrowserClient(window.__APOLLO_STATE__)

const RenderTree = () => (
  <App>
    <ApolloProvider client={client}>
      <BrowserRouter>
        <Routes />
      </BrowserRouter>
    </ApolloProvider>
  </App>
)

ReactDOM[module.hot ? 'render' : 'hydrate'](<RenderTree />, document.getElementById('app'))

if (module.hot) {
  module.hot.accept('./Routes.jsx', function () {
    console.log('App tree changed. Reloading! 🔫')
    ReactDOM[module.hot ? 'render' : 'hydrate'](<RenderTree />, document.getElementById('app'))
  })
}
