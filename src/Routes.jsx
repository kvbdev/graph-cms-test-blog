import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Loadable from 'react-loadable'

import { NotFound } from './components/NotFound'

const LoadingTemp = () => <span>Loading...</span>
LoadingTemp.displayName = 'LoadingSpinner'

const routes = [
  {
    path: '/',
    component: Loadable({
      loader: () => import('./containers/BlogContainer'),
      loading: LoadingTemp
    }),
    exact: true
  },
  {
    path: '/counter',
    component: Loadable({
      loader: () => import('./containers/CounterContainer'),
      loading: LoadingTemp
    })
  },
  {
    component: NotFound
  }
]

export const Routes = () => (
  <Switch>{routes.map((route, index) => <Route key={index} {...route} />)}</Switch>
)
