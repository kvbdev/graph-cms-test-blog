import React from 'react'
import Typography from 'typography'
import CodePlugin from 'typography-plugin-code'
import doelgerTheme from 'typography-theme-doelger'
import styled, { createGlobalStyle } from 'styled-components'
import { Normalize } from 'styled-normalize'

doelgerTheme.plugins = [
  new CodePlugin()
]

const typography = new Typography(doelgerTheme)

const GlobalStyle = createGlobalStyle`
  ${typography.toString()}

  body {
    min-height: calc(100vh - 2em);
    padding: 1em;
    background-repeat: no-repeat;
    background-color: #21D4FD;
    background-image: linear-gradient(19deg, #21D4FD 0%, #B721FF 100%);
  }
`

const AppStyled = styled.div`
  padding: 1em;
  background: white;
  border-radius: 0.5em;
`

export const App = ({ children }) => (
  <>
    <AppStyled>
      {children}
    </AppStyled>
    <Normalize />
    <GlobalStyle />
  </>
)
