import gql from 'graphql-tag'

export default gql`
  mutation updateCounter($count: Int!) {
    updateCounter(count: $count) @client
  }
`
