import React from 'react'
import { shallow } from 'enzyme'
import { NotFound } from './NotFound'

it('matches its snapshot', () => {
  expect(shallow(<NotFound />)).toMatchSnapshot()
})
