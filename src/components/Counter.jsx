import React from 'react'
import PropTypes from 'prop-types'
import { Button } from '../elements'

export const Counter = ({ count, updateCount }) => (
  <React.Fragment>
    <h1>{count}</h1>
    <Button className="incrementCounter" onClick={() => updateCount(count + 1)}>Increment</Button>
    <Button className="decrementCounter" onClick={() => updateCount(count - 1)}>Decrement</Button>
  </React.Fragment>
)

Counter.propTypes = {
  count: PropTypes.number.isRequired,
  updateCount: PropTypes.func.isRequired
}
