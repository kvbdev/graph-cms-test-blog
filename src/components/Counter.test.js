import React from 'react'
import { shallow } from 'enzyme'
import { Counter } from './Counter'

const setup = parameters => {
  const defaults = {
    count: 0,
    updateCount: () => {}
  }

  const props = { ...defaults, ...parameters }
  return shallow(<Counter {...props} />)
}

describe('Counter', () => {
  it('matches its snapshot', () => {
    const component = setup({ count: 123 })
    expect(component).toMatchSnapshot()
  })

  it('renders count passed in props', () => {
    const component = setup({ count: 123 })
    expect(component.find('h1').text()).toBe('123')
  })

  it('calls increment callback on increment button click', () => {
    const updateCount = jest.fn()
    const component = setup({ updateCount })
    component.find('Button.incrementCounter').simulate('click')
    expect(updateCount.mock.calls).toHaveLength(1)
  })

  it('calls decrement callback on decrement button click', () => {
    const updateCount = jest.fn()
    const component = setup({ updateCount })
    component.find('Button.decrementCounter').simulate('click')
    expect(updateCount.mock.calls).toHaveLength(1)
  })
})
