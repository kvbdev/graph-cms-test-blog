import styled from 'styled-components'

export default styled.button`
  padding: 0.5em
`
