import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { withClientState } from 'apollo-link-state'
import { ApolloLink } from 'apollo-link'
import fetch from 'isomorphic-fetch'

const defaults = {
  counter: {
    __typename: 'Counter',
    count: 123
  },
  test: {
    __typename: 'Test',
    count: 456
  }
}

const resolvers = {
  Mutation: {
    updateCounter: (_, { count }, { cache }) => {
      const data = {
        counter: {
          __typename: 'Counter',
          count
        }
      }

      cache.writeData({ data })
      return null
    }
  }
}

const getState = (initialState = {}) => {
  const cache = new InMemoryCache().restore(initialState)

  const stateLink = withClientState({ cache, defaults, resolvers })
  const httpLink = createHttpLink({
    uri: 'https://api-euwest.graphcms.com/v1/cjogxharm09f501fux4hrgp7k/master',
    headers: {
      authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ2ZXJzaW9uIjoxLCJ0b2tlbklkIjoiNzE4Y2JjNzAtMDZjNi00MzI3LTg1YzQtNTRjMWY2Nzg3NzcyIn0.BO3pfY-0sCrrT-wVNaEco9ZsLTOOghvCWDkI7rhskdY`
    },
    fetch
  })

  return {
    link: ApolloLink.from([
      stateLink,
      httpLink
    ]),
    cache
  }
}

export const createServerClient = () => {
  const { link, cache } = getState()
  return new ApolloClient({ link, cache, ssrMode: true })
}

export const createBrowserClient = (initialState = {}) => {
  const { link, cache } = getState(initialState)
  return new ApolloClient({ link, cache, ssrForceFetchDelay: 100 })
}
