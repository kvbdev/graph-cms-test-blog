export default (window) => !(
  typeof window !== 'undefined' &&
  window.document &&
  window.document.createElement
)
