import isServer from './isServer'

describe('isServer utility', () => {
  it('is not a server when required window objects are set', () => {
    const window = { document: { createElement: {} } }
    expect(isServer(window)).toBe(false)
  })

  it('is a server when missing one of the window objects', () => {
    const window = { document: {} }
    expect(isServer(window)).toBe(true)
  })

  it('is a server if window is undefined', () => {
    expect(isServer(undefined)).toBe(true)
  })
})
