import { Counter } from '../components/Counter'
import { graphql, compose } from 'react-apollo'
import getCount from '../queries/counter/getCount'
import updateCounter from '../queries/counter/updateCounter'

const CounterContainer = compose(
  graphql(updateCounter, {
    props: ({ mutate }) => {
      return ({
        updateCount: (count) => {
          return mutate({ variables: { count } })
        }
      })
    }
  }),
  graphql(getCount, {
    props: ({ data: { counter: { count } } }) => ({
      count
    })
  })
)(Counter)

export default CounterContainer
