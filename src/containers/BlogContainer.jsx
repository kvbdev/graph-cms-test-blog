import React from 'react'
import Markdown from 'react-markdown'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import { Link } from 'react-router-dom'

const BlogContainer = () => (
  <Query
    query={gql`
      {
        blogPosts {
          id,
          body,
          title,
          postAssets {
            id
            url
          }
        }
      }
    `}
  >
    {({ loading, error, data }) => {
      if (loading) return <p>Loading...</p>
      if (error) return <p>Error :(</p>

      return (
        <>
          <Link to="/counter">Go To Counter</Link>
          {data.blogPosts.map(({ id, title, body, postAssets }) => {
            for (const asset of postAssets) {
              body = body.replace(asset.id, asset.url)
            }

            return (
              <div key={id}>
                <h1>
                  {title}
                </h1>

                <Markdown source={body} />

                <hr />
              </div>
            )
          })}
        </>
      )
    }}
  </Query>
)

export default BlogContainer
