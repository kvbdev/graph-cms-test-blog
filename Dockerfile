FROM node:11.6.0-alpine
WORKDIR /usr/src/app
COPY package.json ./
COPY npm-shrinkwrap.json ./
RUN npm install
COPY . .
EXPOSE 8080
CMD ["npm", "run", "express"]