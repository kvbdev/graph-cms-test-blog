# KVBDev blog

## Prerequisites

* Node.js - Minimum 8.12 LTS 

## Installation

    npm i

## Building & Running

To run a production build `npm run build`. Assets are sent to `dist/` and this is what you will want to deploy.

During development you'll want to be running the dev server. This will automatically update the page as you make changes to files. The server will run on localhost:8080. To start this server you will need to run `npm start`. This server using `ReactDOM.render` to render the root component and supports hot reloading.

To test server-side JS and server-side rendering when run `npm run express`. The will reload the server and client on change but will also run in production mode. We need to find a solution to update client with dev mode.

## Testing

`npm run test` will test all components in this project. The initial setup came from [this article](https://medium.com/codeclan/testing-react-with-jest-and-enzyme-20505fec4675). We have enzyme to filter out the virtual dom. Snapshots ensure rendering in components don't change between changes.

## Coding standards

We will use https://standardjs.com/. To use with Visual Studio Code you need to install the ESLint plugin. After you do so and install the `node_modules` it should pick up on it. We will be adding some git hooks to reject code that doesn't conform to these standards.

The Visual Studio Code plugin also allows you to fix all autofixable problems for the current file. Just `Ctrl + P` and search for 'eslint' to see all commands.

There is also an npm command to run a report `npm run lint`.
