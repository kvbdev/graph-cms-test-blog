module.exports = {
  extends: ['standard', 'plugin:react/recommended', 'plugin:jest/recommended'],
  parser: 'babel-eslint',
  settings: {
    react: {
      pragma: 'React',
      version: '16.5'
    }
  }
};
 